package controllers;
import play.mvc.Controller;
import play.mvc.Result;
import java.util.List;

import play.data.Form;
import java.util.ArrayList;
import models.Product;
import models.Tag;
import models.StockItem;
import views.html.products.list;
import views.html.products.details;
import play.db.ebean.Model;
import com.avaje.ebean.Ebean;

public class Products extends Controller{
	private static final Form<Product> productForm = Form.form(Product.class);
	
	
	public static Result list(){
		List<Product> products = Product.findAll();
		return 	ok(list.render(products));
	}

	public static Result newProduct(){
		return ok(details.render(productForm));
	}
	
	public static Result details(Product product){
		//wwfinal Product product = Product.findByEan(ean);
		if (product == null) {
		  return notFound(String.format("Product %s does not exist.", product.ean));
		}
		Form<Product> filledForm = productForm.fill(product);
		return ok(details.render(filledForm));
	}
	
	public static Result save(){
		Form<Product> boundForm = productForm.bindFromRequest();
		 if (boundForm.hasErrors()) {
			flash("error", "Please correct the form below.");
			return badRequest(details.render(boundForm));
		 }
		Product product = boundForm.get();
		Ebean.save(product);
		StockItem stockItem = new StockItem();
		stockItem.product = product;
		stockItem.quantity = 0L;
		
		product.save();
		stockItem.save();
		List<Tag> tags = new ArrayList<Tag>();
		for (Tag tag : product.tags) {
		  if (tag.id != null) {
			tags.add(Tag.findById(tag.id));
		  }
		}
		product.tags = tags;
		
		if(product.id == null){
			product.save();
		}else{
			product.update();
		}
		flash("success", String.format("Successfully added product %s", product));
		
		return redirect(routes.Products.list());
	}
	
	public static Result delete(String ean) {
		  final Product product = Product.findByEan(ean);
		  if(product == null) {
			return notFound(String.format("Product %s does not exists.", ean));
		  }
		  //Product.remove(product);
		  product.delete();
		  return redirect(routes.Products.list());
	}

}