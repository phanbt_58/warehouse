package models;
 
 
import javax.persistence.*;
import play.db.ebean.Model;
import play.data.validation.Constraints;
import java.util.*;
 
 @Entity
public class StockItem extends Model{
	@Id
	public Long id;
	
	@ManyToOne
    public Warehouse warehouse;     	// trường quan hệ nối với Warehouse
	
	@ManyToOne
    public Product product;     	// trường quan hệ nối với Product
	
    public Long quantity;
 
    public String toString() {
        return String.format("$d %s", quantity, product);
    }
}