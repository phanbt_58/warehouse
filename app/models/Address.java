package models;

import models.Warehouse;
import javax.persistence.*;

@Entity
public class Address{
	@Id
	public Long id;
	
	@OneToOne(mappedBy = "address")
	public Warehouse warehouse; 
	
	public String street;
	public String number;
	public String city;
	public String country;
	public String postalCode;
}