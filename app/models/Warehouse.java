package models;
 
 
import javax.persistence.*;
import play.db.ebean.Model;
import play.data.validation.Constraints;
import java.util.*;
import models.Address;
 
 @Entity
public class Warehouse extends Model{
	@Id
	public Long id;
	
	public String name;
	
	@OneToOne
	public Address address;
	
	@OneToMany(mappedBy = "warehouse")
	public List<StockItem> stock = new ArrayList();
	
	public String toString(){
		return name;
	}
}